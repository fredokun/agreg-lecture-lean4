
/-

  PART 1 - 4 : **Type classes**
  ==============================

-/

-- Typeclasses have been pioneered by the Haskell programming language
-- for two main (interrelated) reasons :

-- **(1) enable *ad-hoc* polymorphism**

-- Example : the max function

#eval max 42 34
#eval max 3.4 2.9

#check max

#print Max
/-
class Max (α : Type u) where
  /-- The maximum operation: `max x y`. -/
  max : α → α → α
-/

inductive PNat where
  | Z : PNat
  | S (n : PNat) : PNat
deriving Repr

open PNat

def pmax (m n : PNat) : PNat :=
  match m with
  | Z => n
  | S m => match n with
           | Z => S m
           | S n => S (pmax m n)

#eval pmax (S (S (S Z))) (S (S Z))
#eval pmax (S (S Z)) Z
#eval pmax Z (S (S Z))

instance: Max PNat where
  max := pmax

-- Application : The maximum element of a list ?

def max_list {α : Type} [Max α] (le : α → α → Bool) (mx : α) (xs : List α)  : α :=
  match xs with
  | [] => mx
  | x::xs => if le mx x then max_list le x xs
             else max_list le mx xs

#eval max_list (fun (x y : Int) => x ≤ y) 0 [1, 4, 3, 5, 2, 3]
#eval max_list (fun (x y : Float) => x ≤ y) 0 [1.2, 1.3, 3.2, 3.19, 2.42]

def ple (m n : PNat) : Bool :=
  match m with
  | Z => true
  | S m => match n with
           | Z => false
           | S n => ple m n

#eval max_list ple Z [S Z, S (S (S (S Z))), S (S Z), Z]

-- **(2) formalize algebraic structures**
-- (in Mathematics, cf. Mathlib)

-- **Example : Categories**

-- In programming :  only signatures
class Category (cat : Type u → Type u → Type v) where
  id : cat α α
  comp : {α β γ : Type u} → cat β γ → cat α β → cat α γ

infixr:90 " (.) " => Category.comp

-- Example :  functions form a category
instance: Category (·→·) where
  id := id
  comp := (·∘·)

-- In mathematics : laws

class LawfulCategory (cat : Type u → Type u → Type v) [instC: Category cat] where
  id_right (f : cat α β): f (.) Category.id = f
  id_left (f : cat α β): Category.id (.) f = f
  id_assoc {α β γ δ} (f : cat γ δ) (g : cat β γ) (h : cat α β):
    f (.) (g (.) h) = (f (.) g) (.) h


-- Example : functions form a lawful category

instance: LawfulCategory (·→·) where
  id_right _ := rfl
  id_left _ := rfl
  id_assoc _ _ _ := rfl


-- Example : Mealey machines

structure Mealey (s : Type) (α β : Type) where
  act : s → α → (β × s)

def id_Mealey : Mealey s α α :=
  { act := fun st x => (x, st) }

def comp_Mealey (m₁ : Mealey s β γ) (m₂ : Mealey s α β) : Mealey s α γ :=
  { act := fun st x => let (y, st') := m₂.act st x
                       m₁.act st' y
  }

instance: Category (Mealey s) where
  id := id_Mealey
  comp := comp_Mealey

instance: LawfulCategory (Mealey s) where
  id_right _ := rfl --by simp [Category.comp, comp_Mealey, Category.id, id_Mealey]
  id_left _ := rfl
  id_assoc _ _ _ := rfl
