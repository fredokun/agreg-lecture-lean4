
/-

  PART 1 - 1 : **an appetizer : programs, properties and proofs**
  ===============================================================

-/

-- Lean4 is first and foremost ...

--     ... a typed functional programming language

-- Related : ML (SML/Ocaml),  Haskell, Scala, ...

-- Example of a function :

def concat {α : Type} (xs ys : List α) : List α :=
  match xs with
  | [] => ys
  | x::xs => x :: concat xs ys

#eval concat [1, 2, 3] [4, 5, 6]

example:
  concat [1, 2, 3] [4, 5, 6] = [1, 2, 3, 4, 5, 6] :=
by
  rfl

-- Another function using concat :

def rev {α : Type} (xs : List α) : List α :=
  match xs with
  | [] => []
  | x::xs => concat (rev xs) [x]

example:
  rev [1, 2, 3, 4, 5, 6] = [6, 5, 4, 3, 2, 1] :=
by
  rfl

-- Properties of rev ? ... idempotency

example:
  rev (rev [1, 2, 3, 4, 5, 6]) = [1, 2, 3, 4, 5, 6] :=
by
  rfl

theorem rev_concat:
  rev (concat xs [x]) = x :: rev xs :=
by
  induction xs
  case nil => rfl
  case cons y ys Hind =>
    simp [rev, concat]
    rw [Hind]
    simp [concat]

theorem rev_rev_idem {α : Type} (xs : List α):
  rev (rev xs) = xs :=
by
  induction xs
  case nil =>
    simp [rev]
  case cons x xs Hind =>
    simp [rev, concat]
    rw [rev_concat]
    rw [Hind]
