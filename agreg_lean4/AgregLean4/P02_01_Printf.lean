
/-

  PART 2 - 1 : **Safe printf**
  ============================

**Note** : this example is heavily inspired by a similar example in the Idris book

-/

/-

Here is a typical use of `printf` in the C programming language :

    printf("Hello %d world %s!", 42, "of wonders");

This is knowingly an unsafe function, in that various runtime errors
may occur while executing a printf, such as :

    printf("Hello %d world %s!", 42);
    // ==> not enough pas arguments

    printf("Hello %d world %s!", 42, "of wonders", true);
    // ==> too many arguments

    printf("Hello %d world %s!", 42, true);
    // ==> ill-typed arguments

Dependent types offer an elegant solution to the problem of checking
printf arguments at compile-time => a safe printf !

-/

-- **Step 1: a formalization of format strings**

inductive Format where
  | num: Format → Format -- numerical argument
  | str: Format → Format -- string argument
  | lit: String → Format → Format -- literal
  | endf: Format -- end of format string
deriving Repr

open Format

-- The format of  "Hello %d world %s!"
#check lit "Hello " (num (lit " world " (str (lit "!" endf))))
-- : Format

-- **Step 2: constructing the argument-types from the format

def formatType : Format → Type
  | num fmt => Int → formatType fmt
  | str fmt => String → formatType fmt
  | lit _ fmt => formatType fmt
  | endf => String

example: formatType (num endf) = (Int → String)  := by rfl
example: formatType (lit "toto" endf) = String  := by rfl
example: formatType (lit "Hello " (num (lit " world " (str (lit "!" endf)))))
         = (Int → String → String) := by rfl

-- **Step 3: from format string to formats**

def buildFormat : List Char → Format
  | [] => endf
  | '%'::'d'::chars => num (buildFormat chars)
  | '%'::'s'::chars => str (buildFormat chars)
  | '%'::'%'::chars => lit "%" (buildFormat chars)
  | c::chars => match buildFormat chars with
                  | lit l fmt => lit (c.toString ++ l) fmt
                  | fmt => lit c.toString fmt

example: buildFormat [] = endf := by rfl
example: buildFormat ['h', 'e', ' ', '%', 'd']
         = lit "he " (num endf) := by rfl
example: buildFormat (['%', '%', 'd'] ++ " est un littéral".toList)
         = lit "%" (lit "d est un littéral" endf)  := by rfl
example: buildFormat "Hello %d world %s!".toList
         = (lit "Hello " (num (lit " world " (str (lit "!" endf))))) := by rfl

-- **Step 4 : the printf function**

def printfAux (fmt : Format) (acc : String): formatType fmt :=
  match fmt with
  | num fmt => fun i => printfAux fmt (acc ++ (toString i))
  | str fmt => fun s => printfAux fmt (acc ++ s)
  | lit l fmt => printfAux fmt (acc ++ l)
  | endf => acc

def printf (fmt : String): formatType (buildFormat fmt.toList) :=
  printfAux _ ""

example: printf "hello world!" = "hello world!" := by rfl
example: (printf "hello %d world!") 42
         = "hello 42 world!" := by rfl
example: (printf "hello %d world %s!") 42 "of wonders"
         = "hello 42 world of wonders!" := by rfl

/- Ill-typed formats -/
-- #check (printf "hello %d world!") "bad"
-- #check (printf "hello %d world %s!") 42 12

/- too few or two many arguments-/
#check ((printf "hello %d world %s!") 42)
-- #check ((printf "hello %d world %s!") 42 "of wonders" true)
