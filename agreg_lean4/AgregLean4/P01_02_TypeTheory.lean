/-

  PART 1 - 2 : **a tiny little bit of (Type) Theory**
  ===================================================

Disclaimer : this is not a lecture ...

-/

-- Type Theory ≈ λ-calculus with dependent types
-- (most of the time, with added features)


-- Ingredients of a typed lambda-calculus :

/- --------------------------------------
  1) **Atomic values** (and their type)
-------------------------------------- -/

#check 42

#check true

/- ----------------------------------------------------
  2) (total) **Anonymous functions** (with variables)
---------------------------------------------------- -/

-- classical notation : λ x. 2 x

#check fun (x : Nat) => 2 * x

/- -----------------------------
  3) **Definitional features**
----------------------------- -/

def double (x : Nat) := 2 * x
-- (almost) the same as:
def double' := fun (x : Nat) => 2 * x

#check @double
#check @double'

#eval double 4
#eval double' 4

/- -------------------------------------------------
  4) **Other constructions** : tuples, lists, etc.
------------------------------------------------- -/

#check (42, true, "hello")
#check [1, 2, 3, 4]
#check ["hello", "world"]


/- ------------------------
  5) **Types** (as terms)
------------------------- -/
-- **Important** : unlike in most other functional programming language
--                 types are not in a separate namespace
--           ==> they are *terms* of type `Type`

#check Nat
#check Bool
#check (Nat → Nat)
#check Nat × Bool × String

-- Remark : the type of a type is `Type`
--     ... what is the type of `Type` ?

#check Type
#check Type 0
#check Type 1
#check Type 2

-- >>> Lean4 has a (infinite) hierarchy of **universes**

universe u
#check Type u

-- (this is to avoid logical paradoxes ... but that's another story)

-- There is also a special case for types corresponding
-- to logical properties (without going into too much details)
#check Prop

#check 2 ≠ 3
#check 2 = 3  -- Remark : a property is not "inherently true"  ...


/- ----------------------------
 6) **Polymorphic functions**
---------------------------- -/
-- (parametric polymorphism is a natural consequence
--  of types being terms ...)

-- e.g. : a generic identity function
def identity₁ (A : Type) (x : A) := x

#eval identity₁ Nat 42
#eval identity₁ String "hello"

-- ... it's better to make `A` an **implicit argument**

def identity {A : Type} (x : A) := x

#eval identity 42  -- type `Nat` is inferred
#eval identity "Hello" -- type `String` is inferred

-- explicit

#eval @identity Nat 42
#eval @identity String "hello"

/- ------------------------
 7) a "secret" ingredient
------------------------- -/

-- What is the type of the generic identity ?

#check @identity₁

-- The construction :

#check (A : Type) → A → A

-- ... is called : the **dependent product**
-- ... and is tightly related to **universal quantification**

#check ∀ (A : Type), A → A

-- ... with an obvious  logical interpretation ..

-- **Important** :  the "arrow type" is a special case on non-dependent product

#check Nat → Bool

-- ... is the same as :

#check (n : Nat) → Bool   -- with `n` unused at the right of the arrow

/- ---------------------------------------------
 8) From logic to computation (and vice-versa)
--------------------------------------------- -/

-- One of the fundamental rules of logic is the Modus Ponens

--    A → B       A
--    ============= (mp)
--           B

-- This is a trivial theorem in type theory and Lean4

theorem mp (A B : Prop):
  (A → B) → A → B :=
by
  intros f x
  apply f x


-- Hence : Modus ponens is simply function application
-- ... which can be show more explicitly

theorem mp' (A B : Prop):
  (A → B) → A → B := fun f x => f x

-- Important :  we used a function to prove a theorem
--  => the statement (property) is a type
--  => the proof is a term of the very same type

-- ==> this is the essence of the **Curry Howard** correspondence !

--     as a summary :   Properties as Types
--                               Proofs as Terms
--    (colloquially ... Logic ≈ Computation)

-- ... it is in general easier to use the proof *tactics*  (by ...)
--     (at least for students)
