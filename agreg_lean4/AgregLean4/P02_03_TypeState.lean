
/-

  PART 2 - 3 : **Type states**
  ============================

We end the presentation with an abstraction enabled by
dependent types : the manipulation of types with an
evolving state, a.k.a. *behavioral types*

-/

-- We consider the example of a simple simulation system
-- for an automatic door

-- During the simulation, we record the number of times
-- the door has been opened and the number of times
-- someone rang the door bell

structure Door where
  nb_opens : Nat
  nb_rings : Nat
deriving Repr

def initDoor := Door.mk 0 0

-- We begin with an "unsafe" version of the system

-- First we specify the allowed commands to control the door
inductive DoorCmd : Type where
  | dopen : DoorCmd  -- ouverture de la porte
  | dclose : DoorCmd -- fermeture
  | dring : DoorCmd  -- activation de la sonnette
  /- chainage de commandes -/
  | dchain (cmd₁ : DoorCmd) (cmd₂ : DoorCmd):
      DoorCmd
deriving Repr

open DoorCmd

-- And we introduce a dedicated notation for chaining
-- commands
infixr:80 " :> " => dchain

#check dring :> dopen :> dclose

-- And here is a possible implementation of the simulator :

@[simp]
def simulator₁ (door : Door): DoorCmd → Door
  | dopen => { door with nb_opens := door.nb_opens + 1}
  | dclose => door
  | dring => { door with nb_rings := door.nb_rings + 1}
  | dchain cmd₁ cmd₂ => let door' := simulator₁ door cmd₁
                        simulator₁ door' cmd₂


/-

Now, we would like to distinguish between *allowed* and
*disallowed* sequences

-/

-- Examples of allowed sequences
-- exemples :
#eval simulator₁ initDoor (dring :> dopen :> dclose)

#eval simulator₁ initDoor (dopen :> dclose :> dring)

-- Disallowed sequences :
-- double open
#eval simulator₁ initDoor (dring :> dopen :> dopen)
-- double close
#eval simulator₁ initDoor (dopen :> dclose :> dring :> dclose)
-- not open
#eval simulator₁ initDoor (dring :> dclose)
-- ring while open
#eval simulator₁ initDoor (dopen :> dring :> dclose)

/-

It is clear that the simulator is *unsafe* : it allows
the disabled sequences

-/

/-

Using a "normal" programming language, we can avoid so-called
"bad states" by making the simulator a kind of a
*state machine*.

-/

-- First, we need an expicit state
structure DoorState where
  door : Door
  isOpen : Bool
deriving Repr

open DoorState

def initState : DoorState := DoorState.mk initDoor false

-- And now the simulator is defined as a set of state transitions
def simulator₂ : DoorState → DoorCmd → Option DoorState
  | { door := d, isOpen := false }, dopen
    => some { door := { d with nb_opens := d.nb_opens + 1 }, isOpen := true }
  | { door := d, isOpen := true }, dclose
    => some { door := d, isOpen := false}
  | { door :=d, isOpen := false }, dring
    => some { door := { d with nb_rings := d.nb_rings + 1 }, isOpen := false }
  | st, cmd₁ :> cmd₂
    => match simulator₂ st cmd₁ with
       | none => none
       | some st₁ => simulator₂ st₁ cmd₂
  | _, _ => none

-- Allowed sequences
#eval simulator₂ initState (dring :> dopen :> dclose)

#eval simulator₂ initState (dopen :> dclose :> dring)

-- Disallowed sequences :
-- double open
#eval simulator₂ initState (dring :> dopen :> dopen)
-- double close
#eval simulator₂ initState (dopen :> dclose :> dring :> dclose)
-- not open
#eval simulator₂ initState (dring :> dclose)
-- ring while open
#eval simulator₂ initState (dopen :> dring :> dclose)

/-

This is better, disallowed sequences lead to the "impossible"
  (none) state.

However, programming with dependent types enables a safer
approach : disallowing the construction of "bad states"
  at compile-time.

-/

-- The basic idea is to consider the door state as a
-- so-called type state

inductive DoorTypeState := | doorClosed | doorOpen
deriving Repr

open DoorTypeState

-- Next, we constrained the control commands wrt.
-- the type state... Put in other terms,
-- control commands are now "Type transitions"
-- with a  Pre-type-state and and Post-type-state
inductive DoorCmdSafe: DoorTypeState → DoorTypeState → Type where
  | dsopen: DoorCmdSafe doorClosed doorOpen
  | dsclose: DoorCmdSafe doorOpen doorClosed
  | dsring: DoorCmdSafe doorClosed doorClosed
  | dschain (cmd₁ : DoorCmdSafe pre₁ post₁) (cmd₂ : DoorCmdSafe post₁ post₂):
      DoorCmdSafe pre₁ post₂
deriving Repr

open DoorCmdSafe

infixr:80 " ::> " => dschain

-- Now, the type-checker distinguishes the allowed
-- vs. the allowed sequences

--#check dsring ::> dsopen ::> dsclose
--#check dsring ::> dsopen ::> dsopen
--#check dsring ::> dsopen
--#check dsopen ::> dsclose ::> dsring
--#check dsopen ::> dsring ::> dsclose
--#check dsopen ::> dsclose ::> dsring ::> dsclose
--#check dsring ::> dsopen ::> dsclose ::> dsring
--#check dsring ::> dsring ::> dsopen ::> dsclose
--#check dsring ::> dsclose

-- The simulator does not have to hanle "bad states" anymore
def simulator₃ (door : Door) {pre post : DoorTypeState}: DoorCmdSafe pre post → Door
  | dsopen => { door with nb_opens := door.nb_opens + 1}
  | dsclose => door
  | dsring => { door with nb_rings := door.nb_rings + 1}
  | dschain cmd₁ cmd₂ => let door' := simulator₃ door cmd₁
                         simulator₃ door' cmd₂

-- Remark : the pre/post type states are implicit so the
-- simulator is as simple as the first, unsafe one.

-- Allowed sequences
#eval simulator₃ initDoor (dsring ::> dsopen ::> dsclose)

#eval simulator₃ initDoor (dsopen ::> dsclose ::> dsring)

-- Disallowed sequences ...

-- double open
-- #eval simulator₃ initDoor (dsring ::> dsopen ::> dsopen)
-- double close
-- #eval simulator₃ initDoor (dsopen ::> dsclose ::> dsring ::> dsclose)
-- not open
-- #eval simulator₃ initDoor (dsring ::> dsclose)
-- ring while open
-- #eval simulator₃ initDoor (dsopen ::> dsring ::> dsclose)

-- ... are not compilable ...
