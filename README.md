
# A Tutorial Introduction of Dependently-typed programming
# and the Lean4 Theorem Prover

## Author : Frédéric Peschanski - Sorbonne University - LIP6 - APR Team
(frederic.Peschanski@lip6.fr)

## Summary

Based on a lecture for second-year Master's students at Sorbonne
University, I will give a hands-on -- turorial-style -- presentation of
the Lean4 functional programming language and proof assistant.

From the Lean4 website: https://leanprover.github.io

    Lean is a functional programming language that makes it easy to
    write correct and maintainable code.
    You can also use Lean as an interactive theorem prover.


The talk will mainly consists in two complementary parts.
First, I will illustrate the basic principles of the language by means
of simple mathematical constructions such as (typed) sets, natural numbers, etc.
I will also illustrate the way formal properties  and proofs can be specified
and elaborated for such constructions.
 In a second part, I will show a few programming examples that take advantage
of the dependent type system to specify semantic properties about their behaviour.
I will first demonstrate the principles of external verification,
 in which programs, properties and proofs are kept separate. I will also shed
 some lights about internal verification, in which the correctness properties
 (and proofs) are inherent parts of programs. These properties and proofs,
 whether they are internal or external, are verified at compile-time by the
 Lean4 type checker.

The presentation assumes some familiarity with discrete mathematics
(sets, functions, ...) and programming.
Though not strictly required, basic knowledge of typed functional programming
(in e.g. Ocaml, Haskell or Scala) shall be useful.

## Contents of the presentation


 - introduction slides : <https://gitlab.com/fredokun/agreg-lecture-lean4/-/blob/main/Agreg_Seminar_Lean4.pdf>

### Part 1 : Lean4 from the ground up

  1. an Appetizer: <https://gitlab.com/fredokun/agreg-lecture-lean4/-/blob/main/agreg_lean4/AgregLean4/P01_01_Appetizer.lean>
   
  2. the dependly-typed λ-calculus: <https://gitlab.com/fredokun/agreg-lecture-lean4/-/blob/main/agreg_lean4/AgregLean4/P01_02_TypeTheory.lean>
   
  3. Inductive types: <https://gitlab.com/fredokun/agreg-lecture-lean4/-/blob/main/agreg_lean4/AgregLean4/P01_03_Inductives.lean>
   
  4. Typeclasses : <https://gitlab.com/fredokun/agreg-lecture-lean4/-/blob/main/agreg_lean4/AgregLean4/P01_04_Typeclasses.lean>
   
### Part 2 : Programming with dependent types

  1. Safe printf: <https://gitlab.com/fredokun/agreg-lecture-lean4/-/blob/main/agreg_lean4/AgregLean4/P02_01_Printf.lean>

  2. Dependent vectors: <https://gitlab.com/fredokun/agreg-lecture-lean4/-/blob/main/agreg_lean4/AgregLean4/P02_02_Vectors.lean>
  
  3. Typestates: <https://gitlab.com/fredokun/agreg-lecture-lean4/-/blob/main/agreg_lean4/AgregLean4/P02_03_TypeState.lean>

**Note** : to "play" with this contents, you can start vscode (`code .`) or emacs from within the `agreg_lean4` subdirectory.

----
Copyright © 2024 Frederic Peschanski under the Creative Commons CC-BY-SA 4.0 licence
(cf. `legalcode.txt`)
